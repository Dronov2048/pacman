//
//  helpers.h
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#ifndef __pacman__helpers__
#define __pacman__helpers__

#include <SDL2/SDL.h>

//Загружает SDL, SDL_image и создает окно
bool init(SDL_Window** gWindow, /* Окно для отрисовки */
          SDL_Renderer** gRenderer, /* Объект, занимающийся отрисовкой (Рендер) */
          const int SCREEN_WIDTH, /* Высота окна */
          const int SCREEN_HEIGHT); /* Ширина окна */

#endif /* defined(__pacman__helpers__) */

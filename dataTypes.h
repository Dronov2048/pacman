//
//  dataTypes.h
//  pacman_xcode
//
//  Created by Dmitry Basavin on 23.05.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#ifndef pacman_xcode_dataTypes_h
#define pacman_xcode_dataTypes_h

// Структура для произвольных координат
struct Coords {
  int x;
  int y;
};

// Структура для размеров объекта
struct Dimensions {
  int width;  // Ширина
  int height; // Высота
};

// Структура для хранения хитбокса объекта
struct HitBox {
  int x;
  int y;
  int w;
  int h;
};

#endif /* defined(pacman_xcode_dataTypes_h) */

//
//  SuperCoin.cpp
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#include "SuperCoin.h"

SuperCoin::SuperCoin(SDL_Renderer* renderer, Coords pos):VisibleObject(renderer, pos, "sprites/super_coin.png"){
  clName = "SuperCoin";
  
  position = pos;
  
  dimensions.width = 25;
  dimensions.height = 25;
  
  spriteClips = new SDL_Rect[3];
  
  framesCount = 3;
  
  spriteClips[ 0 ].x = 0;
  spriteClips[ 0 ].y = 0;
  spriteClips[ 0 ].w = 25;
  spriteClips[ 0 ].h = 25;
  
  spriteClips[ 1 ].x = 25;
  spriteClips[ 1 ].y = 0;
  spriteClips[ 1 ].w = 25;
  spriteClips[ 1 ].h = 25;
  
  spriteClips[ 2 ].x = 50;
  spriteClips[ 2 ].y = 0;
  spriteClips[ 2 ].w = 25;
  spriteClips[ 2 ].h = 25;
}

SuperCoin::~SuperCoin(){
  delete [] spriteClips;
}

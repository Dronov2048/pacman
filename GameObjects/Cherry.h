//
//  Cherry.h
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#ifndef __pacman__Cherry__
#define __pacman__Cherry__

#include "VisibleObject.h"

// Класс Вишенки
class Cherry: public VisibleObject {
public:
  Cherry(SDL_Renderer* renderer, Coords pos);
  ~Cherry();
};

#endif /* defined(__pacman__Cherry__) */

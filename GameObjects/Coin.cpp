//
//  Coin.cpp
//  pacman
//
//  Created by Dmitry Basavin on 19.04.15.
//  Copyright (c) 2015 Дмитрий Басавин. All rights reserved.
//

#include "Coin.h"

Coin::Coin(SDL_Renderer* renderer, Coords pos):VisibleObject(renderer, pos, "sprites/coin.png"){
  clName = "Coin";
  
  dimensions.width = 25;
  dimensions.height = 25;
  
  spriteClips = new SDL_Rect[6];
  
  framesCount = 6;
  
  spriteClips[ 0 ].x = 0;
  spriteClips[ 0 ].y = 0;
  spriteClips[ 0 ].w = 25;
  spriteClips[ 0 ].h = 25;
  
  spriteClips[ 1 ].x = 25;
  spriteClips[ 1 ].y = 0;
  spriteClips[ 1 ].w = 25;
  spriteClips[ 1 ].h = 25;
  
  spriteClips[ 2 ].x = 50;
  spriteClips[ 2 ].y = 0;
  spriteClips[ 2 ].w = 25;
  spriteClips[ 2 ].h = 25;
  
  spriteClips[ 3 ].x = 75;
  spriteClips[ 3 ].y = 0;
  spriteClips[ 3 ].w = 25;
  spriteClips[ 3 ].h = 25;
  
  spriteClips[ 4 ].x = 100;
  spriteClips[ 4 ].y = 0;
  spriteClips[ 4 ].w = 25;
  spriteClips[ 4 ].h = 25;
  
  spriteClips[ 5 ].x = 125;
  spriteClips[ 5 ].y = 0;
  spriteClips[ 5 ].w = 25;
  spriteClips[ 5 ].h = 25;
}

Coin::~Coin(){
  delete[] spriteClips;
}
